#!/usr/bin/python3

import argparse
from classes.modules import Modules


class Setup:
    def __init__(self):
        self.process_flags()

    @staticmethod
    def process_flags():
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-p', '--profile', help="Specify a Profile to Install")
        group.add_argument('-m', '--module', help="Specify a Module to Install")
        args = parser.parse_args()
        mod = Modules()
        if args.profile:
            mod.set_profile(args.profile)
            mod.install()
        elif args.module:
            mod.set_module(args.module)
            mod.install()
        else:
            parser.print_help()


if __name__ == "__main__":
    Setup()

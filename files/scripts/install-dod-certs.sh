#!/bin/bash

setup () {
	WORKING_DIR="$(/usr/bin/mktemp -d)"
	FIREFOX_DIR=$(/usr/bin/find ~/.mozilla/firefox/ -name "cert9.db" -exec dirname {} \;)
	ZIP_FILE="${WORKING_DIR}/AllCerts.zip"
	ZIP_SUM="d39c8da31a7aebebacaa4437fffcc76fd12004e56d9277795dc78ce52b228499"
}

download () {
	pushd ${WORKING_DIR}
		/usr/bin/wget https://militarycac.com/maccerts/AllCerts.zip -O ${ZIP_FILE}
		if [ "$(/usr/bin/sha256sum AllCerts.zip | /usr/bin/awk '{ print $1 }')" != "${ZIP_SUM}" ]; then
			echo "Invalid Checksum! The AllCerts.zip file has changed... Exiting..."
			exit 1
		fi
	popd
}

unzip () {
	/usr/bin/unzip ${ZIP_FILE} -d ${WORKING_DIR}
}

install_firefox () {
	for cert in ${WORKING_DIR}/*.cer; do
		name=$(basename -s .cer "${cert}")
		/usr/bin/certutil -A -n "${name}" -t "CT,c," -i "${cert}" -d "${FIREFOX_DIR}"
	done
}

install_linux () {
	sudo cp "${WORKING_DIR}/*.cer" /etc/ca-certificates/extracted/cadir
	sudo trust extract-compat
}

cleanup () {
	rm -r ${WORKING_DIR}
}

main () {
	setup
	download
	unzip
	install_firefox
	cleanup
}

main

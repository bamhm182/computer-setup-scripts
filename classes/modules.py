#!/usr/bin/python3

import json
import os
from classes.installer import Installer


class Modules:
    def __init__(self):
        self.modules = []

    def set_profile(self, profile):
        profile = os.path.join("files/profiles", profile + ".json")
        self.check_file(profile)
        with open(profile) as json_file:
            for module in json.load(json_file)["modules"]:
                self.set_module(module)

    def set_module(self, module):
        module = os.path.join("files/modules", module + ".json")

        self.check_file(module)
        self.modules.append(module)

    @staticmethod
    def check_file(file):
        if not os.path.isfile(file):
            print(f"Not Found: {file}")
            exit()

    def install(self):
        i = Installer()
        for module in self.modules:
            i.install(module)

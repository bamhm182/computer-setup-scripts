#!/usr/bin/python3

import json
import subprocess
from classes.phases.pre import Pre
from classes.phases.main import Main
from classes.phases.post import Post


class Installer:
    def __init__(self):
        self.managers = []
        self.find_managers()

    def find_managers(self):
        for manager in ["pamac", "pacman", "apt"]:
            try:
                if not subprocess.check_call(["which", manager], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL):
                    self.managers.append(manager)
            except subprocess.CalledProcessError:
                pass

    def install(self, file):
        with open(file) as json_file:
            module = json.load(json_file)

            if not subprocess.call(["bash", "-c", module["check"]]):
                print(f"{module['name']} is already installed...")
            else:
                print(f"Installing {module['name']}...")
                if "pre" in module:
                    Pre(module["pre"])
                if "main" in module:
                    Main(self.managers, module["main"])
                if "post" in module:
                    Post(module["post"])

#!/usr/bin/python

import subprocess


class Main:
    def __init__(self, managers, install):
        if "pacman" in install and "pamac" in managers:
            subprocess.call(["pacman", "-S", "--noconfirm"] + install["pacman"])
        if "pamac" in install and "pamac" in managers:
            subprocess.call(["pamac", "install", "--no-confirm"] + install["pamac"])
        if "apt" in install and "apt" in managers:
            subprocess.call(["sudo", "apt", "install", "-y"] + install["apt"])

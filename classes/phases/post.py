#!/usr/bin/python3

import os
import shutil
import subprocess


class Post:
    def __init__(self, post):
        if "shell" in post:
            self.shell(post["shell"])
        if "files" in post:
            self.files(post["files"])

    @staticmethod
    def shell(commands):
        for cmd in commands:
            subprocess.call(cmd, shell=True, executable="/bin/bash")

    @staticmethod
    def files(files):
        for file in files:
            if file["mode"] == "new":
                shutil.copyfile(file["source"], os.path.expanduser(file["file"]))
            elif file["mode"] == "append":
                with open(file["file"], "a+") as f:
                    f.write(file["content"])
            elif file["mode"] == "link":
                os.symlink(file["source"], file["file"])

#!/usr/bin/python3

import os
import subprocess


class Pre:
    def __init__(self, pre):
        if "env" in pre:
            self.env(pre["env"])

    @staticmethod
    def env(variables):
        for env in variables:
            output = subprocess.Popen(env["value"], shell=True, executable="/bin/bash",
                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
            os.environ[env["name"]] = output[0].decode("UTF-8")